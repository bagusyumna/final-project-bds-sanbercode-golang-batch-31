package controllers

import (
	"html"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"

	"final-project-bds-sanbercode-golang-batch-31/models"
	"final-project-bds-sanbercode-golang-batch-31/utils/token"
)

// Login User godoc
// @Summary Login as as user.
// @Description Logging in to get jwt token to access admin or user api by roles.
// @Tags Auth
// @Param Body body models.LoginInput true "the body to login a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /login [post]
func Login(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input models.LoginInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{}
	u.User_username = input.Username

	_, err := u.GetUserByUsername(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	}

	err = token.VerifyPassword(input.Password, u.User_password)

	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Password dan Username Tidak Cocok"})
		return
	}

	token, err := token.GenerateToken(u.ID)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	user := map[string]string{
		"id":       strconv.Itoa(int(u.ID)),
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "login success", "user": user, "token": token})

}

// Register godoc
// @Summary Register a user.
// @Description registering a user from public access.
// @Tags Auth
// @Param Body body models.RegisterInput true "the body to register a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /register [post]
func Register(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input models.RegisterInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{}

	u.User_email = input.Email
	u.Created_at = time.Now()
	u.Updated_at = time.Now()

	roleValid := map[string]bool{"admin": true, "seller": true, "buyer": true}

	if roleValid[input.Role] {
		u.User_role = input.Role
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Role user hanya : Admin, Seller dan Buyer"})
		return
	}

	hashedPassword, errPassword := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)
	if errPassword != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": errPassword.Error()})
		return
	}

	u.User_username = html.EscapeString(strings.TrimSpace(input.Username))
	u.User_password = string(hashedPassword)

	_, err := u.SaveUser(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": input.Username,
		"email":    input.Email,
	}

	c.JSON(http.StatusOK, gin.H{"message": "registration success", "user": user})

}

// Change Password User godoc
// @Summary Change Password User.
// @Description Change Password User.
// @Tags Auth
// @Param Body body models.ChangePassInput true "the body to change password user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /changepassword [post]
func ChangePass(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input models.ChangePassInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{}

	u.User_username = input.Username

	_, err := u.GetUserByUsername(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = token.VerifyPassword(input.OldPassword, u.User_password)

	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		c.JSON(http.StatusBadRequest, gin.H{"error": "password lama tidak cocok"})
		return
	}

	hashedPassword, errPassword := bcrypt.GenerateFromPassword([]byte(input.NewPassword), bcrypt.DefaultCost)
	if errPassword != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": errPassword.Error()})
		return
	}

	u.User_password = string(hashedPassword)
	u.Updated_at = time.Now()

	// simpan
	_, err = u.UpdateUser(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
	}

	c.JSON(http.StatusOK, gin.H{"message": "change password success", "user": user})

}
