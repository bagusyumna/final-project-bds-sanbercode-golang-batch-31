package controllers

import (
	"final-project-bds-sanbercode-golang-batch-31/models"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Get All Product godoc
// @Summary Get All Product
// @Description
// @Tags Product
// @Produce json
// @Success 200 {object} []models.Product
// @Router /product [get]
func GetAllProduct(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	pd := models.Product{}

	productList, err := pd.GetAllProduct(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "product": productList})
}

// Get Detail of a Product godoc
// @Summary Get Detail of a Product
// @Description
// @Tags Product
// @Param product_id path string true "Product ID"
// @Produce json
// @Success 200 {object} models.Product
// @Router /product/detail/{product_id} [get]
func GetProductById(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	product_id := c.Param("product_id")
	i_product_id, _ := strconv.Atoi(product_id)

	pd := models.Product{}

	pd.ID = uint(i_product_id)

	_, err := pd.GetProductById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "product": pd})

}

// Get All Product from a store godoc
// @Summary Get All Product from a store
// @Description
// @Tags Product
// @Param store_id path string true "Store ID"
// @Produce json
// @Success 200 {object} []models.Product
// @Router /product/store/{store_id} [get]
func GetProductByStore(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	store_id := c.Param("store_id")
	i_store_id, _ := strconv.Atoi(store_id)

	st := models.Store{}

	st.ID = uint(i_store_id)

	_, err := st.GetStoreById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Store Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	pd := models.Product{}

	pd.Store_id = st.ID

	productList, err := pd.GetProductByStore(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	stringIdOwner := strconv.Itoa(int(st.User_id))

	store := map[string]string{
		"store_name":  st.Store_name,
		"store_owner": stringIdOwner,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "store": store, "product": productList})
}

// Create a Product godoc
// @Summary Create a Product
// @Description
// @Tags Product
// @Param store_id path string true "Store ID"
// @Param Body body models.AddProductInput true "body"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Product
// @Router /product/store/{store_id} [post]
func PostProductByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	store_id := c.Param("store_id")
	i_store_id, _ := strconv.Atoi(store_id)

	var input models.AddProductInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	st := models.Store{}

	st.ID = uint(i_store_id)

	_, err := st.GetStoreById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Store Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	pd := models.Product{}

	pd.Product_name = input.Product_name
	pd.Product_desc = input.Product_desc
	pd.Product_stock = input.Product_stock
	pd.Product_price = input.Product_price
	pd.Product_photo_url = input.Product_photo_url
	pd.Store_id = uint(i_store_id)
	pd.Category_id = input.Category_id
	pd.Created_at = time.Now()
	pd.Updated_at = time.Now()

	_, err = pd.SaveProduct(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	stringIdOwner := strconv.Itoa(int(st.User_id))

	store := map[string]string{
		"store_name":  st.Store_name,
		"store_owner": stringIdOwner,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success create new product", "store": store, "product": pd})

}

// Update a Product godoc
// @Summary Update a Product
// @Description
// @Tags Product
// @Param store_id path string true "Store ID"
// @Param product_id path string true "Product ID"
// @Param Body body models.UpdateProductInput true "body"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Product
// @Router /product/store/{store_id}/{product_id} [patch]
func UpdateProductByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	store_id := c.Param("store_id")
	i_store_id, _ := strconv.Atoi(store_id)

	product_id := c.Param("product_id")
	i_product_id, _ := strconv.Atoi(product_id)

	st := models.Store{}

	st.ID = uint(i_store_id)

	_, err := st.GetStoreById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Store Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	pd := models.Product{}

	pd.ID = uint(i_product_id)

	_, err = pd.GetProductById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var input models.UpdateProductInput

	if err = c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	pd.Product_name = input.Product_name
	pd.Product_desc = input.Product_desc
	pd.Product_stock = input.Product_stock
	pd.Product_price = input.Product_price
	pd.Product_photo_url = input.Product_photo_url
	pd.Updated_at = time.Now()

	_, err = pd.UpdateProduct(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	stringIdOwner := strconv.Itoa(int(st.User_id))

	store := map[string]string{
		"store_name":  st.Store_name,
		"store_owner": stringIdOwner,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success update new product", "store": store, "product": pd})

}

// Delete a Product godoc
// @Summary Delete a Product
// @Description
// @Tags Product
// @Param store_id path string true "Store ID"
// @Param product_id path string true "Product ID"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Product
// @Router /product/store/{store_id}/{product_id} [delete]
func DeleteProductByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	store_id := c.Param("store_id")
	i_store_id, _ := strconv.Atoi(store_id)

	product_id := c.Param("product_id")
	i_product_id, _ := strconv.Atoi(product_id)

	st := models.Store{}

	st.ID = uint(i_store_id)

	_, err := st.GetStoreById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Store Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	pd := models.Product{}

	pd.ID = uint(i_product_id)

	_, err = pd.GetProductById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, err = pd.DeleteProduct(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	stringIdOwner := strconv.Itoa(int(st.User_id))

	store := map[string]string{
		"store_name":  st.Store_name,
		"store_owner": stringIdOwner,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success delete product", "store": store})
}
