package controllers

import (
	"final-project-bds-sanbercode-golang-batch-31/models"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Get All Category godoc
// @Summary Get All Category
// @Description
// @Tags Category
// @Produce json
// @Success 200 {object} []models.Category
// @Router /category [get]
func GetAllCategory(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	ca := models.Category{}

	categoryList, err := ca.GetAllCategory(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "category": categoryList})
}

// Get Detail of a Category godoc
// @Summary Get Detail of a Category
// @Description
// @Tags Category
// @Param category_id path string true "Category ID"
// @Produce json
// @Success 200 {object} models.Category
// @Router /category/{category_id} [get]
func GetCategoryById(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	category_id := c.Param("category_id")
	i_category_id, _ := strconv.Atoi(category_id)

	ca := models.Category{}

	ca.ID = uint(i_category_id)

	_, err := ca.GetCategoryById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Category Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "category": ca})

}

// Create a Category godoc
// @Summary Create a Category
// @Description
// @Tags Category
// @Param Body body models.AddCategoryInput true "body"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Category
// @Router /category [post]
func PostCategoryByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input models.AddCategoryInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{}

	u.ID = input.User_id

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userValid := map[string]bool{"admin": true}
	if !userValid[u.User_role] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Hanya User Dengan Role Admin yang dapat menambah category"})
		return
	}

	ca := models.Category{}

	ca.Category_name = input.Category_name
	ca.Category_desc = input.Category_desc
	ca.User_id = input.User_id
	ca.Created_at = time.Now()
	ca.Updated_at = time.Now()

	_, err = ca.SaveCategory(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success create new category", "user": user, "category": ca})
}

// Update a Category godoc
// @Summary Update a Category
// @Description
// @Tags Category
// @Param user_id path string true "User ID"
// @Param category_id path string true "Category ID"
// @Param Body body models.UpdateCategoryInput true "body"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Category
// @Router /category/{user_id}/{category_id} [patch]
func UpdateCategoryByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	user_id := c.Param("user_id")
	i_user_id, _ := strconv.Atoi(user_id)

	category_id := c.Param("category_id")
	i_category_id, _ := strconv.Atoi(category_id)

	u := models.User{}

	u.ID = uint(i_user_id)

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userValid := map[string]bool{"admin": true}
	if !userValid[u.User_role] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Hanya User Dengan Role Admin yang dapat mengupdate category"})
		return
	}

	ca := models.Category{}

	ca.ID = uint(i_category_id)

	_, err = ca.GetCategoryById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Category Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var input models.UpdateCategoryInput

	if err = c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ca.Category_name = input.Category_name
	ca.Category_desc = input.Category_desc
	ca.Updated_at = time.Now()

	_, err = ca.UpdateCategory(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success update store", "user": user, "category": ca})

}

// Delete a Category godoc
// @Summary Delete a Category
// @Description
// @Tags Category
// @Param user_id path string true "User ID"
// @Param category_id path string true "Category ID"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Category
// @Router /category/{user_id}/{category_id} [delete]
func DeleteCategoryByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	user_id := c.Param("user_id")
	i_user_id, _ := strconv.Atoi(user_id)

	category_id := c.Param("category_id")
	i_category_id, _ := strconv.Atoi(category_id)

	u := models.User{}

	u.ID = uint(i_user_id)

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userValid := map[string]bool{"admin": true}
	if !userValid[u.User_role] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Hanya User Dengan Role Admin yang dapat mengupdate category"})
		return
	}

	ca := models.Category{}

	ca.ID = uint(i_category_id)

	_, err = ca.GetCategoryById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Category Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, err = ca.DeleteCategory(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success delete category", "user": user})

}
