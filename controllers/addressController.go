package controllers

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"final-project-bds-sanbercode-golang-batch-31/models"
)

// Get All Address godoc
// @Summary Get All Address
// @Description
// @Tags Address
// @Produce json
// @Param user_id path string true "User ID"
// @Success 200 {object} []models.Address
// @Router /address/{user_id} [get]
func GetAllAddressByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	user_id := c.Param("user_id")
	i_user_id, _ := strconv.Atoi(user_id)

	u := models.User{}

	u.ID = uint(i_user_id)

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	add := models.Address{}

	add.User_id = u.ID

	addressList, err := add.GetAllAddressByUser(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "user": user, "address": addressList})
}

// Create Address by user godoc
// @Summary Create Address by user
// @Description
// @Tags Address
// @Param Body body models.AddAddressInput true "body"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Address
// @Router /address [post]
func PostAddressByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input models.AddAddressInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	add := models.Address{}

	add.Address_line = input.Address_line

	activeValid := map[string]bool{"T": true, "F": true}
	if !activeValid[input.Address_active] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Inputan Active Hanya Boleh T dan F"})
		return
	}

	add.Address_active = input.Address_active
	add.User_id = input.User_id
	add.Created_at = time.Now()
	add.Updated_at = time.Now()

	u := models.User{}

	u.ID = input.User_id

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userValid := map[string]bool{"seller": true, "buyer": true}
	if !userValid[u.User_role] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Hanya User Dengan Role Seller dan Buyer yang Diizinkan Input"})
		return
	}

	_, err = add.SaveAddress(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	addressNew := map[string]string{
		"address": input.Address_line,
		"active":  input.Address_active,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success tambah data", "user": user, "address": addressNew})
}

// Update Address by user godoc
// @Summary Update Address by user
// @Description
// @Tags Address
// @Param user_id path string true "User ID"
// @Param add_id path string true "Address ID"
// @Param Body body models.UpdateAddressInput true "body"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Address
// @Router /address/{user_id}/{add_id} [patch]
func UpdateAddressByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	user_id := c.Param("user_id")
	i_user_id, _ := strconv.Atoi(user_id)

	add_id := c.Param("add_id")
	i_add_id, _ := strconv.Atoi(add_id)

	u := models.User{}

	u.ID = uint(i_user_id)

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	add := models.Address{}

	add.ID = uint(i_add_id)

	_, err = add.GetAddressById(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if add.User_id != u.ID {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Adddress Bukan Milih User " + u.User_username})
		return
	}

	var input models.UpdateAddressInput

	if err = c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	add.Address_line = input.Address_line
	add.Address_active = input.Address_active
	add.Updated_at = time.Now()

	_, err = add.UpdateAddress(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success update data", "user": user, "address": add})

}

// Delete Address by user godoc
// @Summary Delete Address by user
// @Description
// @Tags Address
// @Param user_id path string true "User ID"
// @Param add_id path string true "Address ID"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /address/{user_id}/{add_id} [delete]
func DeleteAddressByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	user_id := c.Param("user_id")
	i_user_id, _ := strconv.Atoi(user_id)

	add_id := c.Param("add_id")
	i_add_id, _ := strconv.Atoi(add_id)

	u := models.User{}

	u.ID = uint(i_user_id)

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	add := models.Address{}

	add.ID = uint(i_add_id)

	_, err = add.GetAddressById(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if add.User_id != u.ID {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Adddress Bukan Milih User " + u.User_username})
		return
	}

	_, err = add.DeleteAddress(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success delete data", "user": user})

}
