package controllers

import (
	"final-project-bds-sanbercode-golang-batch-31/models"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Get All Data User Cart godoc
// @Summary All Data User Cart
// @Description
// @Tags Cart
// @Param user_id path string true "User ID"
// @Produce json
// @Success 200 {object} []models.Cart
// @Router /cart/{user_id} [get]
func GetAllCartByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	user_id := c.Param("user_id")
	i_user_id, _ := strconv.Atoi(user_id)

	u := models.User{}

	u.ID = uint(i_user_id)

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userValid := map[string]bool{"buyer": true}
	if !userValid[u.User_role] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Hanya User Dengan Role Buyer yang Diizinkan Melihat Data"})
		return
	}

	ca := models.Cart{}

	ca.Buyer_id = uint(i_user_id)

	cardList, err := ca.GetAllCartByUser(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "user": user, "cart": cardList})
}

// Create data cart godoc
// @Summary Create data cart
// @Description
// @Tags Cart
// @Param Body body models.AddCartInput true "body"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Cart
// @Router /cart [post]
func PostCartByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input models.AddCartInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	checkProduct := models.Cart{}

	checkProduct.Product_id = input.Product_id
	checkProduct.Buyer_id = input.Buyer_id

	_, err := checkProduct.GetCartByProduct(db)

	if checkProduct.ID != 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product Sudah Ada dalam cart"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	pd := models.Product{}

	pd.ID = input.Product_id

	_, err = pd.GetProductById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product Tidak Ditemukan"})
		return
	} else if pd.Store_id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{}

	u.ID = input.Buyer_id

	_, err = u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userValid := map[string]bool{"buyer": true}
	if !userValid[u.User_role] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Hanya User Dengan Role Buyer yang Diizinkan Input"})
		return
	}

	st := models.Store{}

	st.ID = pd.Store_id

	st.GetStoreById(db)

	ca := models.Cart{}

	ca.Product_id = input.Product_id
	ca.Buyer_id = input.Buyer_id
	ca.Product_quantity = input.Product_quantity
	ca.Product_total_price = input.Product_quantity * pd.Product_price
	ca.Product_owner_id = st.User_id
	ca.Created_at = time.Now()
	ca.Updated_at = time.Now()

	_, err = ca.SaveCart(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success added new item to cart", "cart": ca, "user": user})

}

// Update data cart godoc
// @Summary Update data cart
// @Description
// @Tags Cart
// @Param user_id path string true "User ID"
// @Param card_id path string true "Cart ID"
// @Param Body body models.UpdateCartInput true "body"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Cart
// @Router /cart/{user_id}/{card_id} [patch]
func UpdateCartByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	user_id := c.Param("user_id")
	i_user_id, _ := strconv.Atoi(user_id)

	card_id := c.Param("card_id")
	i_card_id, _ := strconv.Atoi(card_id)

	var input models.UpdateCartInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{}

	u.ID = uint(i_user_id)

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userValid := map[string]bool{"buyer": true}
	if !userValid[u.User_role] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Hanya User Dengan Role Buyer yang Diizinkan Update Data"})
		return
	}

	ca := models.Cart{}

	ca.ID = uint(i_card_id)

	_, err = ca.GetCartById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product Tidak Ditemukan Didalam Cart"})
		return
	} else if ca.Buyer_id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product Tidak Ditemukan Didalam Cart"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if ca.Buyer_id != uint(i_user_id) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product Tidak Ditemukan Didalam Cart"})
		return
	}

	pd := models.Product{}

	pd.ID = ca.Product_id

	pd.GetProductById(db)

	ca.Product_quantity = input.Product_quantity
	ca.Product_total_price = pd.Product_price * input.Product_quantity
	ca.Updated_at = time.Now()

	_, err = ca.UpdateCart(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success updated item inside cart", "cart": ca, "user": user})

}

// Delete data cart godoc
// @Summary Delete data cart
// @Description
// @Tags Cart
// @Param user_id path string true "User ID"
// @Param card_id path string true "Cart ID"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Cart
// @Router /cart/{user_id}/{card_id} [delete]
func DeleteCartByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	user_id := c.Param("user_id")
	i_user_id, _ := strconv.Atoi(user_id)

	card_id := c.Param("card_id")
	i_card_id, _ := strconv.Atoi(card_id)

	u := models.User{}

	u.ID = uint(i_user_id)

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userValid := map[string]bool{"buyer": true}
	if !userValid[u.User_role] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Hanya User Dengan Role Buyer yang Diizinkan Delete Data"})
		return
	}

	ca := models.Cart{}

	ca.ID = uint(i_card_id)

	_, err = ca.GetCartById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product Tidak Ditemukan Didalam Cart"})
		return
	} else if ca.Buyer_id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product Tidak Ditemukan Didalam Cart"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if ca.Buyer_id != uint(i_user_id) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product Tidak Ditemukan Didalam Cart"})
		return
	}

	_, err = ca.DeleteCart(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success delete item inside cart", "user": user})

}
