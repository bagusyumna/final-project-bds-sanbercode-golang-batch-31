package controllers

import (
	"final-project-bds-sanbercode-golang-batch-31/models"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Get All Transaction from user godoc
// @Summary Get All Transaction from user
// @Description
// @Tags Transaction
// @Param user_id path string true "User ID"
// @Produce json
// @Success 200 {object} []models.Transaction
// @Router /transaction/{user_id} [get]
func GetAllTransactionByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	user_id := c.Param("user_id")
	i_user_id, _ := strconv.Atoi(user_id)

	u := models.User{}

	u.ID = uint(i_user_id)

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userValid := map[string]bool{"buyer": true}
	if !userValid[u.User_role] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Hanya User Dengan Role Buyer yang Diizinkan Melihat Transaksi"})
		return
	}

	tr := models.Transaction{}

	tr.Buyer_id = uint(i_user_id)

	transactionList, err := tr.GetAllTransactionByUser(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "user": user, "transaction": transactionList})

}

// Get Detail User Transaction godoc
// @Summary Get Detail User Transaction
// @Description
// @Tags Transaction
// @Param user_id path string true "User ID"
// @Param transaction_id path string true "Transaction ID"
// @Produce json
// @Success 200 {object} models.Transaction
// @Router /transaction/{user_id}/{transaction_id} [get]
func GetDetailTransactionByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	user_id := c.Param("user_id")
	i_user_id, _ := strconv.Atoi(user_id)

	transaction_id := c.Param("transaction_id")
	i_transaction_id, _ := strconv.Atoi(transaction_id)

	u := models.User{}

	u.ID = uint(i_user_id)

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userValid := map[string]bool{"buyer": true}
	if !userValid[u.User_role] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Hanya User Dengan Role Buyer yang Diizinkan Melihat Transaksi"})
		return
	}

	tr := models.Transaction{}

	tr.ID = uint(i_transaction_id)

	_, err = tr.GetTransactionById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Transaksi Tidak Ditemukan"})
		return
	} else if tr.Buyer_id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Transaksi Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if tr.Buyer_id != uint(i_user_id) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Transaksi Tidak Ditemukan"})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "user": user, "transaction": tr})

}

// Create User Transaction godoc
// @Summary Create User Transaction
// @Description
// @Tags Transaction
// @Param Body body models.AddTransactionInput true "body"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Transaction
// @Router /transaction [post]
func PostTransactionByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input models.AddTransactionInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	checkTrans := models.Transaction{}
	checkTrans.Buyer_id = input.Buyer_id
	checkTrans.Cart_id = input.Cart_id

	_, err := checkTrans.GetTransactionByCartBuyer(db)

	if checkTrans.ID != 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Sudah Ada Transaksi Berjalan dengan Cart yang sama"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{}

	u.ID = input.Buyer_id

	_, err = u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userValid := map[string]bool{"buyer": true}
	if !userValid[u.User_role] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Hanya User Dengan Role Buyer yang Diizinkan Melanjutkan Transaksi"})
		return
	}

	ca := models.Cart{}

	ca.ID = input.Cart_id

	_, err = ca.GetCartById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Cart Tidak Ditemukan"})
		return
	} else if ca.Buyer_id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Cart Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if ca.Buyer_id != input.Buyer_id {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Cart Tidak Ditemukan"})
		return
	}

	tr := models.Transaction{}

	tr.Buyer_id = input.Buyer_id
	tr.Cart_id = input.Cart_id
	tr.Total_price = ca.Product_total_price
	tr.Payment = input.Payment
	tr.Status_payment = "F"
	tr.Created_at = time.Now()
	tr.Updated_at = time.Now()

	_, err = tr.SaveTransactionByUser(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success melakukan transaksi", "transaction": tr, "user": user})

}

// Update User Transaction godoc
// @Summary Update User Transaction
// @Description
// @Tags Transaction
// @Param user_id path string true "User ID"
// @Param transaction_id path string true "Transaction ID"
// @Param Body body models.UpdateTransactionInput true "body"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Transaction
// @Router /transaction/{user_id}/{transaction_id} [patch]
func UpdateTransactionByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	user_id := c.Param("user_id")
	i_user_id, _ := strconv.Atoi(user_id)

	transaction_id := c.Param("transaction_id")
	i_transaction_id, _ := strconv.Atoi(transaction_id)

	var input models.UpdateTransactionInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{}

	u.ID = uint(i_user_id)

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userValid := map[string]bool{"buyer": true}
	if !userValid[u.User_role] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Hanya User Dengan Role Buyer yang Diizinkan Melanjutkan Transaksi"})
		return
	}

	tr := models.Transaction{}

	tr.ID = uint(i_transaction_id)

	_, err = tr.GetTransactionById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Transaksi Tidak Ditemukan"})
		return
	} else if tr.Buyer_id == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Transaksi Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if tr.Buyer_id != uint(i_user_id) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Transaksi Tidak Ditemukan"})
		return
	}

	activeValid := map[string]bool{"T": true, "F": true}
	if !activeValid[input.Status_payment] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Inputan Payment Status Hanya Boleh T dan F"})
		return
	}

	tr.Status_payment = input.Status_payment
	tr.Updated_at = time.Now()

	_, err = tr.UpdateTransactionByUser(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success update transaksi", "transaction": tr, "user": user})
}
