package controllers

import (
	"final-project-bds-sanbercode-golang-batch-31/models"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Get All Store godoc
// @Summary Get All Store
// @Description
// @Tags Store
// @Produce json
// @Success 200 {object} []models.Store
// @Router /store [get]
func GetAllStore(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	st := models.Store{}

	storeList, err := st.GetAllStore(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "stores": storeList})

}

// Get Detail Store Data godoc
// @Summary Get Detail Store Data
// @Description
// @Tags Store
// @Produce json
// @Param store_id path string true "Store ID"
// @Success 200 {object} models.Store
// @Router /store/{store_id} [get]
func GetStoreById(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	store_id := c.Param("store_id")
	i_store_id, _ := strconv.Atoi(store_id)

	st := models.Store{}

	st.ID = uint(i_store_id)

	_, err := st.GetStoreById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Store Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{}

	u.ID = st.User_id

	_, err = u.GetUserById(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "user": user, "store": st})

}

// Create a Store godoc
// @Summary Create a Store
// @Description
// @Tags Store
// @Param Body body models.AddStoreInput true "body"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Store
// @Router /store [post]
func PostStoreByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input models.AddStoreInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{}

	u.ID = input.User_id

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userValid := map[string]bool{"seller": true}
	if !userValid[u.User_role] {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Hanya User Dengan Role Seller yang dapat membuat store"})
		return
	}

	st := models.Store{}

	st.Store_name = input.Store_name
	st.Store_phone = input.Store_phone
	st.Store_bank = input.Store_bank
	st.Store_url_logo = input.Store_url_logo
	st.User_id = input.User_id
	st.Created_at = time.Now()
	st.Updated_at = time.Now()

	stCheck := models.Store{}

	stCheck.User_id = input.User_id
	_, err = stCheck.GetStoreByUserId(db)
	if err == nil && stCheck.ID != 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "satu seller hanya dibolehkan membuat 1 store"})
		return
	}

	_, err = st.SaveStore(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success create new store", "user": user, "store": st})

}

// Update a Store godoc
// @Summary Update a Store
// @Description
// @Tags Store
// @Param user_id path string true "User ID"
// @Param store_id path string true "Store ID"
// @Param Body body models.UpdateStoreInput true "body"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Store
// @Router /store/{user_id}/{store_id} [patch]
func UpdateStoreByUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	user_id := c.Param("user_id")
	i_user_id, _ := strconv.Atoi(user_id)

	store_id := c.Param("store_id")
	i_store_id, _ := strconv.Atoi(store_id)

	u := models.User{}

	u.ID = uint(i_user_id)

	_, err := u.GetUserById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	st := models.Store{}

	st.ID = uint(i_store_id)

	_, err = st.GetStoreById(db)

	if err != nil && err == gorm.ErrRecordNotFound {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Store Tidak Ditemukan"})
		return
	} else if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if st.User_id != u.ID {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Store bukan milik user : " + u.User_username})
		return
	}

	var input models.UpdateStoreInput

	if err = c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	st.Store_name = input.Store_name
	st.Store_phone = input.Store_phone
	st.Store_bank = input.Store_bank
	st.Store_url_logo = input.Store_url_logo
	st.Updated_at = time.Now()

	_, err = st.UpdateStore(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"username": u.User_username,
		"email":    u.User_email,
		"role":     u.User_role,
	}

	c.JSON(http.StatusOK, gin.H{"message": "success update store", "user": user, "store": st})

}
