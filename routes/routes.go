package routes

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"final-project-bds-sanbercode-golang-batch-31/controllers"
	"final-project-bds-sanbercode-golang-batch-31/middlewares"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()

	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	r.POST("/login", controllers.Login)
	r.POST("/register", controllers.Register)
	r.POST("/changepassword", controllers.ChangePass)

	r.GET("/address/:user_id", controllers.GetAllAddressByUser)
	addressesMiddlewareRoute := r.Group("/address")
	addressesMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	addressesMiddlewareRoute.POST("/", controllers.PostAddressByUser)
	addressesMiddlewareRoute.PATCH("/:user_id/:add_id", controllers.UpdateAddressByUser)
	addressesMiddlewareRoute.DELETE("/:user_id/:add_id", controllers.DeleteAddressByUser)

	r.GET("/store", controllers.GetAllStore)
	r.GET("/store/:store_id", controllers.GetStoreById)
	storesMiddlewareRoute := r.Group("/store")
	storesMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	storesMiddlewareRoute.POST("/", controllers.PostStoreByUser)
	storesMiddlewareRoute.PATCH("/:user_id/:store_id", controllers.UpdateStoreByUser)

	r.GET("/category", controllers.GetAllCategory)
	r.GET("/category/:category_id", controllers.GetCategoryById)
	categoryMiddlewareRoute := r.Group("/category")
	categoryMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	categoryMiddlewareRoute.POST("/", controllers.PostCategoryByUser)
	categoryMiddlewareRoute.PATCH("/:user_id/:category_id", controllers.UpdateCategoryByUser)
	categoryMiddlewareRoute.DELETE("/:user_id/:category_id", controllers.DeleteCategoryByUser)

	r.GET("/product", controllers.GetAllProduct)
	r.GET("/product/detail/:product_id", controllers.GetProductById)
	r.GET("/product/store/:store_id", controllers.GetProductByStore)
	productMiddlewareRoute := r.Group("/product")
	productMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	productMiddlewareRoute.POST("/store/:store_id", controllers.PostProductByUser)
	productMiddlewareRoute.PATCH("/store/:store_id/:product_id", controllers.UpdateProductByUser)
	productMiddlewareRoute.DELETE("/store/:store_id/:product_id", controllers.DeleteProductByUser)

	r.GET("/cart/:user_id", controllers.GetAllCartByUser)
	cartMiddlewareRoute := r.Group("/cart")
	cartMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	cartMiddlewareRoute.POST("/", controllers.PostCartByUser)
	cartMiddlewareRoute.PATCH("/:user_id/:card_id", controllers.UpdateCartByUser)
	cartMiddlewareRoute.DELETE("/:user_id/:card_id", controllers.DeleteCartByUser)

	r.GET("/transaction/:user_id", controllers.GetAllTransactionByUser)
	r.GET("/transaction/:user_id/:transaction_id", controllers.GetDetailTransactionByUser)
	transactionMiddlewareRoute := r.Group("/transaction")
	transactionMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	transactionMiddlewareRoute.POST("/", controllers.PostTransactionByUser)
	transactionMiddlewareRoute.PATCH("/:user_id/:transaction_id", controllers.UpdateTransactionByUser)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
