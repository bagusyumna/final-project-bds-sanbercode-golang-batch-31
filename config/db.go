package config

import (
	"final-project-bds-sanbercode-golang-batch-31/utils"
	"fmt"
	"log"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func ConnectDataBase() *gorm.DB {

	environment := utils.Getenv("ENVIRONMENT", "production")

	if environment == "production" {

		username := os.Getenv("DATABASE_USERNAME")
		password := os.Getenv("DATABASE_PASSWORD")
		host := os.Getenv("DATABASE_HOST")
		port := os.Getenv("DATABASE_PORT")
		database := os.Getenv("DATABASE_NAME")

		dsn := "host=" + host + " user=" + username + " password=" + password + " dbname=" + database + " port=" + port + " sslmode=require"
		db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
		if err != nil {
			log.Fatal("Prod : ", err.Error())
			panic(err.Error())
		}

		// db.AutoMigrate(&models.User{}, &models.Movie{}, &models.AgeRatingCategory{})

		return db
	} else {

		host := utils.Getenv("DATABASE_HOST", "")
		port := utils.Getenv("DATABASE_PORT", "")
		database := utils.Getenv("DATABASE_NAME", "")
		username := utils.Getenv("DATABASE_USERNAME", "")
		password := utils.Getenv("DATABASE_PASSWORD", "")

		dsn := fmt.Sprintf("postgres://%v:%v@%v:%v/%v", username, password, host, port, database)

		db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

		if err != nil {
			log.Fatal("Dev : ", err.Error())
			panic(err.Error())
		}

		return db
	}

}
