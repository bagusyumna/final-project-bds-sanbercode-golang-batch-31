package main

import (
	"final-project-bds-sanbercode-golang-batch-31/config"
	"final-project-bds-sanbercode-golang-batch-31/docs"
	"final-project-bds-sanbercode-golang-batch-31/routes"
	"final-project-bds-sanbercode-golang-batch-31/utils"
)

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @termsOfService http://swagger.io/terms/

func main() {
	// for load godotenv
	// for env
	// err := godotenv.Load()
	// if err != nil {
	// 	log.Fatal("Error loading .env file")
	// }

	// programmatically set swagger info
	docs.SwaggerInfo.Title = "Dokumentasi Final Project BDS Sanbercode Golang Batch 31"
	docs.SwaggerInfo.Description = "Tema ecommerce (benchmark tokopedia)"
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = utils.Getenv("SWAGGER_HOST", "final-project-sanbercode.herokuapp.com")
	docs.SwaggerInfo.Schemes = []string{"https"}

	// database connection
	db := config.ConnectDataBase()
	sqlDB, _ := db.DB()
	defer sqlDB.Close()

	// router
	r := routes.SetupRouter(db)
	// just remove port 8080
	r.Run()
}
