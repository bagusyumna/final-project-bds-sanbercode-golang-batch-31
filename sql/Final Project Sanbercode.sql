CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "user_email" varchar,
  "user_username" varchar,
  "user_password" varchar,
  "user_role" varchar,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "addresses" (
  "id" SERIAL PRIMARY KEY,
  "address_line" varchar,
  "address_active" char,
  "user_id" int,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "stores" (
  "id" SERIAL PRIMARY KEY,
  "store_name" varchar,
  "store_phone" varchar,
  "store_bank" int,
  "store_url_logo" varchar,
  "user_id" int,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "products" (
  "id" SERIAL PRIMARY KEY,
  "product_name" varchar,
  "product_desc" varchar,
  "product_stock" int,
  "product_price" int,
  "product_photo_url" varchar,
  "store_id" int,
  "category_id" int,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "categories" (
  "id" SERIAL PRIMARY KEY,
  "category_name" varchar,
  "category_desc" varchar,
  "user_id" int,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "carts" (
  "id" SERIAL PRIMARY KEY,
  "product_id" int,
  "buyer_id" int,
  "product_quantity" int,
  "product_total_price" int,
  "product_owner_id" int,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "transactions" (
  "id" SERIAL PRIMARY KEY,
  "buyer_id" int,
  "cart_id" int,
  "total_price" int,
  "payment" varchar,
  "status_payment" char,
  "created_at" timestamp,
  "updated_at" timestamp
);

ALTER TABLE "addresses" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "store" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "products" ADD FOREIGN KEY ("store_id") REFERENCES "store" ("id");

ALTER TABLE "products" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");

ALTER TABLE "categories" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "carts" ADD FOREIGN KEY ("product_id") REFERENCES "products" ("id");

ALTER TABLE "carts" ADD FOREIGN KEY ("buyer_id") REFERENCES "users" ("id");

ALTER TABLE "carts" ADD FOREIGN KEY ("product_owner_id") REFERENCES "users" ("id");

ALTER TABLE "transactions" ADD FOREIGN KEY ("buyer_id") REFERENCES "users" ("id");

ALTER TABLE "transactions" ADD FOREIGN KEY ("cart_id") REFERENCES "carts" ("id");

