package models

import (
	"time"

	"gorm.io/gorm"
)

type (
	Store struct {
		ID             uint
		Store_name     string
		Store_phone    string
		Store_bank     uint
		Store_url_logo string
		User_id        uint
		Created_at     time.Time
		Updated_at     time.Time
	}
)

func (st *Store) SaveStore(db *gorm.DB) (*Store, error) {
	var err error = db.Create(&st).Error
	if err != nil {
		return &Store{}, err
	}
	return st, nil
}

func (st *Store) UpdateStore(db *gorm.DB) (*Store, error) {
	var err error = db.Model(&st).Updates(st).Error
	if err != nil {
		return &Store{}, err
	}
	return st, nil
}

func (st *Store) GetStoreById(db *gorm.DB) (*Store, error) {
	var err error = db.Find(&st, "id = ?", st.ID).Error
	if err != nil {
		return &Store{}, err
	}
	return st, nil
}

func (st *Store) GetStoreByUserId(db *gorm.DB) (*Store, error) {
	var err error = db.Find(&st, "user_id = ?", st.User_id).Error
	if err != nil {
		return &Store{}, err
	}
	return st, nil
}

func (st *Store) GetAllStore(db *gorm.DB) ([]Store, error) {
	var stores []Store
	var err error = db.Find(&stores).Error
	if err != nil {
		return stores, err
	}
	return stores, nil

}
