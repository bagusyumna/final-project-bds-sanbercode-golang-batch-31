package models

type LoginInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type RegisterInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
	Email    string `json:"email" binding:"required"`
	Role     string `json:"role" binding:"required"`
}

type ChangePassInput struct {
	Username    string `json:"username" binding:"required"`
	OldPassword string `json:"old_password" binding:"required"`
	NewPassword string `json:"new_password" binding:"required"`
}

type AddAddressInput struct {
	Address_line   string `json:"address" binding:"required"`
	Address_active string `json:"active" binding:"required"`
	User_id        uint   `json:"user" binding:"required"`
}

type UpdateAddressInput struct {
	Address_line   string `json:"address" binding:"required"`
	Address_active string `json:"active" binding:"required"`
}

type AddStoreInput struct {
	Store_name     string `json:"store_name" binding:"required"`
	Store_phone    string `json:"store_phone" binding:"required"`
	Store_bank     uint   `json:"store_bank" binding:"required"`
	Store_url_logo string `json:"store_url_logo" binding:"required"`
	User_id        uint   `json:"user" binding:"required"`
}

type UpdateStoreInput struct {
	Store_name     string `json:"store_name" binding:"required"`
	Store_phone    string `json:"store_phone" binding:"required"`
	Store_bank     uint   `json:"store_bank" binding:"required"`
	Store_url_logo string `json:"store_url_logo" binding:"required"`
}

type AddCategoryInput struct {
	Category_name string `json:"category_name" binding:"required"`
	Category_desc string `json:"category_phone" binding:"required"`
	User_id       uint   `json:"user" binding:"required"`
}

type UpdateCategoryInput struct {
	Category_name string `json:"category_name" binding:"required"`
	Category_desc string `json:"category_phone" binding:"required"`
}

type AddProductInput struct {
	Product_name      string `json:"product_name" binding:"required"`
	Product_desc      string `json:"product_desc" binding:"required"`
	Product_stock     uint   `json:"product_stock" binding:"required"`
	Product_price     uint   `json:"product_price" binding:"required"`
	Product_photo_url string `json:"product_photo_url" binding:"required"`
	Category_id       uint   `json:"category" binding:"required"`
}

type UpdateProductInput struct {
	Product_name      string `json:"product_name" binding:"required"`
	Product_desc      string `json:"product_desc" binding:"required"`
	Product_stock     uint   `json:"product_stock" binding:"required"`
	Product_price     uint   `json:"product_price" binding:"required"`
	Product_photo_url string `json:"product_photo_url" binding:"required"`
}

type AddCartInput struct {
	Product_id       uint `json:"product_id" binding:"required"`
	Buyer_id         uint `json:"buyer" binding:"required"`
	Product_quantity uint `json:"product_quantity" binding:"required"`
}

type UpdateCartInput struct {
	Product_quantity uint `json:"product_quantity" binding:"required"`
}

type AddTransactionInput struct {
	Buyer_id uint   `json:"buyer" binding:"required"`
	Cart_id  uint   `json:"cart_id" binding:"required"`
	Payment  string `json:"payment_method" binding:"required"`
}

type UpdateTransactionInput struct {
	Status_payment string `json:"payment_status" binding:"required"`
}
