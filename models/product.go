package models

import (
	"time"

	"gorm.io/gorm"
)

type (
	Product struct {
		ID                uint
		Product_name      string
		Product_desc      string
		Product_stock     uint
		Product_price     uint
		Product_photo_url string
		Store_id          uint
		Category_id       uint
		Created_at        time.Time
		Updated_at        time.Time
	}
)

func (pd *Product) SaveProduct(db *gorm.DB) (*Product, error) {
	var err error = db.Create(&pd).Error
	if err != nil {
		return &Product{}, err
	}
	return pd, nil
}

func (pd *Product) UpdateProduct(db *gorm.DB) (*Product, error) {
	var err error = db.Model(&pd).Updates(pd).Error
	if err != nil {
		return &Product{}, err
	}
	return pd, nil
}

func (pd *Product) DeleteProduct(db *gorm.DB) (*Product, error) {
	var err error = db.Delete(&pd).Error
	if err != nil {
		return &Product{}, err
	}
	return pd, nil
}

func (pd *Product) GetProductById(db *gorm.DB) (*Product, error) {
	var err error = db.Find(&pd, "id = ?", pd.ID).Error
	if err != nil {
		return &Product{}, err
	}
	return pd, nil
}

func (pd *Product) GetProductByStore(db *gorm.DB) ([]Product, error) {
	var products []Product
	var err error = db.Find(&products, "store_id = ?", pd.Store_id).Error
	if err != nil {
		return products, err
	}
	return products, nil
}

func (pd *Product) GetAllProduct(db *gorm.DB) ([]Product, error) {
	var products []Product
	var err error = db.Find(&products).Error
	if err != nil {
		return products, err
	}
	return products, nil
}
