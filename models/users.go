package models

import (
	"time"

	"gorm.io/gorm"
)

type (
	User struct {
		ID            uint      `json:"id"`
		User_username string    `json:"username"`
		User_password string    `json:"password"`
		User_email    string    `json:"email"`
		User_role     string    `json:"role"`
		Created_at    time.Time `json:"created_at"`
		Updated_at    time.Time `json:"updated_at"`
	}
)

func (u *User) SaveUser(db *gorm.DB) (*User, error) {
	var err error = db.Create(&u).Error
	if err != nil {
		return &User{}, err
	}
	return u, nil
}

func (u *User) GetUserById(db *gorm.DB) (*User, error) {
	var err error = db.Model(User{}).Where("id = ?", u.ID).Take(&u).Error
	if err != nil {
		return &User{}, err
	}
	return u, nil
}

func (u *User) GetUserByUsername(db *gorm.DB) (*User, error) {
	var err error = db.Model(User{}).Where("user_username = ?", u.User_username).Take(&u).Error
	if err != nil {
		return &User{}, err
	}
	return u, nil
}

func (u *User) UpdateUser(db *gorm.DB) (*User, error) {
	var err error = db.Model(&u).Updates(u).Error
	if err != nil {
		return &User{}, err
	}
	return u, nil
}
