package models

import (
	"time"

	"gorm.io/gorm"
)

type (
	Address struct {
		ID             uint      `json:"id"`
		Address_line   string    `json:"addressLine"`
		Address_active string    `json:"addressActive"`
		User_id        uint      `json:"userId"`
		Created_at     time.Time `json:"created_at"`
		Updated_at     time.Time `json:"updated_at"`
	}
)

func (add *Address) SaveAddress(db *gorm.DB) (*Address, error) {
	var err error = db.Create(&add).Error
	if err != nil {
		return &Address{}, err
	}
	return add, nil
}

func (add *Address) GetAddressById(db *gorm.DB) (*Address, error) {
	var err error = db.Find(&add, "id = ?", add.ID).Error
	if err != nil {
		return &Address{}, err
	}
	return add, nil
}

func (add *Address) GetAllAddressByUser(db *gorm.DB) ([]Address, error) {
	var addresses []Address
	var err error = db.Find(&addresses, "user_id = ?", add.User_id).Error
	if err != nil {
		return addresses, err
	}
	return addresses, nil
}

func (add *Address) UpdateAddress(db *gorm.DB) (*Address, error) {
	var err error = db.Model(&add).Updates(add).Error
	if err != nil {
		return &Address{}, err
	}
	return add, nil
}

func (add *Address) DeleteAddress(db *gorm.DB) (*Address, error) {
	var err error = db.Delete(&add).Error
	if err != nil {
		return &Address{}, err
	}
	return add, nil
}
