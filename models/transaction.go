package models

import (
	"time"

	"gorm.io/gorm"
)

type (
	Transaction struct {
		ID             uint
		Buyer_id       uint
		Cart_id        uint
		Total_price    uint
		Payment        string
		Status_payment string
		Created_at     time.Time
		Updated_at     time.Time
	}
)

func (tr *Transaction) GetAllTransactionByUser(db *gorm.DB) ([]Transaction, error) {
	var transactions []Transaction
	var err error = db.Find(&transactions, "buyer_id = ?", tr.Buyer_id).Error
	if err != nil {
		return transactions, err
	}
	return transactions, nil
}

func (tr *Transaction) GetTransactionById(db *gorm.DB) (*Transaction, error) {
	var err error = db.Find(&tr, "id = ?", tr.ID).Error
	if err != nil {
		return &Transaction{}, err
	}
	return tr, nil
}

func (tr *Transaction) GetTransactionByCartBuyer(db *gorm.DB) (*Transaction, error) {
	var err error = db.Find(&tr, "buyer_id = ? AND cart_id = ?", tr.Buyer_id, tr.Cart_id).Error
	if err != nil {
		return &Transaction{}, err
	}
	return tr, nil
}

func (tr *Transaction) SaveTransactionByUser(db *gorm.DB) (*Transaction, error) {
	var err error = db.Create(&tr).Error
	if err != nil {
		return &Transaction{}, err
	}
	return tr, nil
}

func (tr *Transaction) UpdateTransactionByUser(db *gorm.DB) (*Transaction, error) {
	var err error = db.Model(&tr).Updates(tr).Error
	if err != nil {
		return &Transaction{}, err
	}
	return tr, nil
}
