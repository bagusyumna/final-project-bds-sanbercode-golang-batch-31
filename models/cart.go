package models

import (
	"time"

	"gorm.io/gorm"
)

type (
	Cart struct {
		ID                  uint
		Product_id          uint
		Buyer_id            uint
		Product_quantity    uint
		Product_total_price uint
		Product_owner_id    uint
		Created_at          time.Time
		Updated_at          time.Time
	}
)

func (ca *Cart) SaveCart(db *gorm.DB) (*Cart, error) {
	var err error = db.Create(&ca).Error
	if err != nil {
		return &Cart{}, err
	}
	return ca, nil
}

func (ca *Cart) UpdateCart(db *gorm.DB) (*Cart, error) {
	var err error = db.Model(&ca).Updates(ca).Error
	if err != nil {
		return &Cart{}, err
	}
	return ca, nil
}

func (ca *Cart) DeleteCart(db *gorm.DB) (*Cart, error) {
	var err error = db.Delete(&ca).Error
	if err != nil {
		return &Cart{}, err
	}
	return ca, nil
}

func (ca *Cart) GetCartByProduct(db *gorm.DB) (*Cart, error) {
	var err error = db.Find(&ca, "product_id = ? AND buyer_id = ?", ca.Product_id, ca.Buyer_id).Error
	if err != nil {
		return &Cart{}, err
	}
	return ca, nil
}

func (ca *Cart) GetCartById(db *gorm.DB) (*Cart, error) {
	var err error = db.Find(&ca, "id = ?", ca.ID).Error
	if err != nil {
		return &Cart{}, err
	}
	return ca, nil
}

func (ca *Cart) GetAllCartByUser(db *gorm.DB) ([]Cart, error) {
	var carts []Cart
	var err error = db.Find(&carts, "buyer_id = ?", ca.Buyer_id).Error
	if err != nil {
		return carts, err
	}
	return carts, nil
}
