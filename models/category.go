package models

import (
	"time"

	"gorm.io/gorm"
)

type (
	Category struct {
		ID            uint
		Category_name string
		Category_desc string
		User_id       uint
		Created_at    time.Time
		Updated_at    time.Time
	}
)

func (ca *Category) SaveCategory(db *gorm.DB) (*Category, error) {
	var err error = db.Create(&ca).Error
	if err != nil {
		return &Category{}, err
	}
	return ca, nil
}

func (ca *Category) UpdateCategory(db *gorm.DB) (*Category, error) {
	var err error = db.Model(&ca).Updates(ca).Error
	if err != nil {
		return &Category{}, err
	}
	return ca, nil
}

func (ca *Category) DeleteCategory(db *gorm.DB) (*Category, error) {
	var err error = db.Delete(&ca).Error
	if err != nil {
		return &Category{}, err
	}
	return ca, nil
}

func (ca *Category) GetCategoryById(db *gorm.DB) (*Category, error) {
	var err error = db.Find(&ca, "id = ?", ca.ID).Error
	if err != nil {
		return &Category{}, err
	}
	return ca, nil
}

func (ca *Category) GetAllCategory(db *gorm.DB) ([]Category, error) {
	var categories []Category
	var err error = db.Find(&categories).Error
	if err != nil {
		return categories, err
	}
	return categories, nil
}
